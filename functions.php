<?php
/**
 * skeleton-theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package skeleton-theme
 */

/**
  * @throws \InvalidArgumentException
  */
	function mix($file)
{
		static $manifest = null;

		if (is_null($manifest)) {
			$url = get_template_directory() . '/dist/mix-manifest.json';
				$manifest = json_decode(file_get_contents($url ), true);
		}

		// die('<pre>'.print_r($manifest, true). '</pre>');

		if (isset($manifest[$file])) {
				return '/'.$manifest[$file];
		}

		trigger_error(sprintf('mix-manifest.json no existe', $file), E_USER_ERROR);
}

/**
 * Clean up the_excerpt()
 */
function apt_excerpt_more() {
  return ' &hellip; <a id="seguir-leyendo" href="' . get_permalink() . '">' . __('Seguir leyendo', 'apt') . '<i class="i-arrow-right"></i></a>';
}
add_filter('excerpt_more', 'apt_excerpt_more');

if ( ! function_exists( 'asociacion_para_todos_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function asociacion_para_todos_setup() {

		//Add sizes of images
		add_image_size( 'xp-index-post-size', 300, 300, true );
		add_image_size( 'noticias-post-size', 660, 450, true );
		add_image_size( 'sub-header-size', 400, 175, true );
		add_image_size( 'xp-single-header-size', 1010, 500, true );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'principal-menu' => esc_html__( 'Principal', 'skeleton-theme' ),
			'social' => esc_html__( 'Social Media Menu', 'skeleton-theme' ),
			'footer-menu-1' => esc_html__( 'Footer 1', 'skeleton-theme' ),
			'footer-menu-2' => esc_html__( 'Footer 2', 'skeleton-theme' ),
			'footer-menu-3' => esc_html__( 'Footer 3', 'skeleton-theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 80,
			'width'       => 80,
			'flex-width'  => true,
			'flex-height' => false,
		) );
	}
endif;
add_action( 'after_setup_theme', 'asociacion_para_todos_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function asociacion_para_todos_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'asociacion_para_todos_content_width', 640 );
}
add_action( 'after_setup_theme', 'asociacion_para_todos_content_width', 0 );


/**
 * Global javascript variables
 */
$directory = array(
	'home_url' => get_home_url(),
);

/**
 * Enqueue scripts and styles.
 */
function asociacion_para_todos_scripts() {
	//Enqueue Google Fonts: Source Montserrat, Sans-serif
	wp_enqueue_style('skeleton-theme-fonts', 'https://fonts.googleapis.com/css?family=Montserrat:200,300,300i,400,400i,500,600,700');

	//Font Types
	wp_enqueue_style('skeleton-theme-fonts', 'https://fonts.googleapis.com/css?family=Noto+Sans:400,700');

	//Swipper
	wp_enqueue_style('skeleton-theme-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.css');
	wp_enqueue_style('skeleton-theme-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css');
	wp_enqueue_script('skeleton-theme-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.js', ['jquery'], null, true);
	wp_enqueue_script('skeleton-theme-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js', ['jquery'], null, true);
	wp_enqueue_script('skeleton-theme-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.jquery.js', ['jquery'], null, true);
	wp_enqueue_script('skeleton-theme-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.jquery.min.js', ['jquery'], null, true);

	//Stylesheet
	wp_enqueue_style( 'skeleton-theme-style',  get_template_directory_uri() . '/dist' . mix('/css/styles.css'));

	//Main Script
	wp_enqueue_script( 'asociacion_para_todos_scripts',  get_template_directory_uri() . '/dist' . mix('/js/main.js'), ['jquery'], null, true);

	wp_enqueue_script( 'skeleton-theme-navigation', get_template_directory_uri() . '/dist' . mix('/js/navigation.js'), array(), null, true );

	wp_enqueue_script( 'skeleton-theme-skip-link-focus-fix', get_template_directory_uri() . '/dist' . mix('/js/skip-link-focus-fix.js'), array(), null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'asociacion_para_todos_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


//----------------MIS FUNCIONES-------------------

/**
 * Ingresando los scripts ajenos al original.
 */
require get_template_directory() . '/my-functions/mis-scripts.php';

/**
 * Sidebar and widget area.
 */
require get_template_directory() . '/my-functions/sidebar_widget-zone.php';

/**
 * Cambiando los titulos de las paginas.
 */
require get_template_directory() . '/my-functions/titulos.php';

/**
 * Creando los diferentes post-types.
 */
require get_template_directory() . '/my-functions/post-types.php';

/**
 * Creando las diferentes taxonomias para el wp-admin.
 */
require get_template_directory() . '/my-functions/taxonomias.php';

/**
 * Modificando la cantidad de palabras que muestra el resumen de un post.
 */
require get_template_directory() . '/my-functions/excerpt-length.php';

/**
 * Reemplazando las casillas vacias de imagenes por una en especifico.
 */
require get_template_directory() . '/my-functions/placeholders.php';

/**
 * Optiones Page Advanced Custom Fields PRO.
 */
require get_template_directory() . '/my-functions/options-page-acf.php';

/**
 * Colors picker ACF PRO.
 */
require get_template_directory() . '/my-functions/colors-palette.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
