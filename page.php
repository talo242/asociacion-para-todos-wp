<?php
/**
 * The template for displaying all informatives pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */

get_header(); 
include('sub-header.php');

?>

<div class="site-main">
	<div class="feed">

			<?php
			if ( have_rows( 'informative_page' ) ):

				get_template_part( 'template-parts/content', 'page' );

			endif;
			?>
	</div>
</div>

<?php

get_footer();
