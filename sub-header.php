<?php
/**
 * The sub-header for our theme
 *
 * This is the template that displays the Titles and a little image after the principal header (header.php).
 *
 * @package asociacion-para-todos
 */
$image;
$color;

if ( 1 == 0 ) {
} elseif ( is_singular('apt_experiencias') ) {

	  $image = get_field('experiencias_sub-header_thumbnail', 'option');
	  $color = get_field('experiencias_sub-header_color', 'option');

} elseif ( is_post_type_archive('apt_experiencias') ||
					 is_page_template( 'responsive-cards.php' ) ) {

		$image = get_field('asociacion_sub-header_thumbnail', 'option');
	  $color = get_field('asociacion_sub-header_color', 'option');

} elseif ( is_post_type_archive('apt_enf_rar') ||
						is_singular( 'apt_enf_rar' ) ||
						get_the_title() == '¿Qué son las ER?' ) {

		$image = get_field('enf_rar_sub-header_thumbnail', 'option');
	  $color = get_field('enf_rar_sub-header_color', 'option');

} elseif ( taxonomy_exists( 'apt_servicios_categorias' ) ||
					 is_singular( 'apt_servicios' ) ) {

		$image = get_field('servicios_sub-header_thumbnail', 'option');
	  $color = get_field('servicios_sub-header_color', 'option');

} elseif ( is_page_template('donaciones.php') ) {

		$image = get_field('donaciones_sub-header_thumbnail', 'option');
	  $color = get_field('donaciones_sub-header_color', 'option');

} elseif ( is_page() ) {

		$image = get_field('informative_page_sub-header_thumbnail', 'option');
	  $color = get_field('informative_page_sub-header_color', 'option');

} elseif( is_single() || is_home() ) {

		$image = get_field('actualidad_sub-header_thumbnail', 'option');
	  $color = get_field('actualidad_sub-header_color', 'option');

}


//$image = get_field('sub-header_thumbnail', 'option');
//$color = get_field('sub-header_color', 'option');
//die('<pre>' . print_r($image, true) . '</pre>');
?>

<div class="sub-header" style="background-color:<?= $color ?>">
	<div class="sub-header-image-container">
		<img class="wp-sub-header-image" src="<?= $image['sizes']['sub-header-size']; ?>">
		<div class="sub-header-blur-div" style="box-shadow: inset 100px 0 150px <?= $color ?>;"></div>
	</div>
	<div id="page-title">
		<h2><?= apt_title(); ?></h2>	
	</div>
</div>
