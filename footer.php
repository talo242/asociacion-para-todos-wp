<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package asociacion-para-todos
 */
$telefono = get_field('telefono', 'option');
$direccion =get_field('direccion', 'option');
$email = get_field('email', 'option');
?>

	<footer id="site-footer" class="site-footer">
	
	<!-- #ayuda footer -->
		<div class="pre-footer-ayuda">
			<div class="inside-container">
				<div class="ayuda-footer-s"><i id="icon-ayuda" class="i-book2"></i>
					<div id="text">
            <h3>¿Quieres saber más?</h3>
            <p>Puedes <a href="#">consultar nuestros artículos</a> sobre las Enfermedades Raras.</p>
           </div> 
          </div>
          <div class="ayuda-footer-s"><i id="icon-ayuda" class="i-lifebuoy"></i>
						<div id="text">
	            <h3>¿Necesitas ayuda?</h3>
	            <p> 
	            	<a href="#">Comunícate con nosotros</a> 
	            	si conoces a alguien que pueda padecer una Enfermedad Rara.
	            </p>
	        	</div> 
	      	</div>
			</div>
		</div>
	
	<!-- #menus footer -->
		<div class="principal-footer-menus">
		<!-- .inside-container -->
			<div class="inside-container">
			<!-- #menu-informate -->
			<div class="footer-section">
				<h3>Infórmate</h3>
				<button class="informate-button">Infórmate</button>
				<nav id="menu-informate" class="menu-informate">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer-menu-1',
							'menu_id'        => 'informate',
						) );
					?>
				</nav><!-- #footer-informate -->
			</div>
			<!-- #menu-participa -->
				<div class="footer-section">
					<h3>Participa</h3>
					<button class="participa-button">Participa</button>
					<nav id="menu-participa" class="menu-participa">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-2',
								'menu_id'        => 'participa',
							) );
						?>
					</nav><!-- #footer-participa -->
				</div>
			<!-- #menu-apoya -->
				<div class="footer-section">
					<h3>Apoya</h3>
					<button class="apoya-button">Apoya</button>
					<nav id="menu-apoya" class="menu-apoya">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu-3',
								'menu_id'        => 'apoya',
							) );
						?>
					</nav><!-- #footer-apoya -->
				</div>
			<!-- #contactanos -->
				<div class="footer-section">
					<h3>Contáctanos</h3>
          <button class="contactanos-button">Contáctanos</button>
          <div class="menu-contacto">
          	<p><a href="tel:<?= $telefono ?>"><i class="i-phone"></i><?= $telefono ?></a></p>
            <p><a><i class="i-location"></i><?= $direccion ?></a></p>
            <p><a href="mailto:<?= $email ?>"><i class="i-envelop"></i><?= $email ?></a></p>
            <p><a href="/quienes-somos"><i class="i-user"></i>¿Quiénes somos?</a></p>
          </div>
        </div>
			</div>
		</div><!-- .principal-footer-menus -->
	
	<!-- #copyright footer -->
		<div class="sub-footer-copyright">
		<a href="index.html"><img src="<?= get_template_directory_uri(); ?>/dist/img/logo-v.png" alt="Asociación Para Todos" class="logo-footer"></a>
      <div class="copyright">
        <div class="license">
          <div class="license-img"><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" target="_blank"></a></div>
          <p>Excepto donde se especifique lo contrario, el contenido de este sitio web está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licencia Creative Commons Atribución-Compartir. Igual 4.0 Internacional.</a></p>
        </div>
        <p>Diseñado y desarrollado con <i class="i-heart"></i>por 
        <a href="https://www.linkedin.com/in/talovelasco/" target="_blank">Ricardo Velasco</a> & 
        <a href="http://www.instagram.com/rodra.gt" target="_blank">Rodrigo Gabriel</a></p>
      </div>
		</div>	
	
	</footer><!-- .site-footer -->
<?php wp_footer(); ?>

</body>
</html>
