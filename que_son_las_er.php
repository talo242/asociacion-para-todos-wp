<?php
/*
Template Name: Tema - Qué son las ER
Description: Colocar específicamente este tema para mostrar el contenido estático de la página ¿Qué son las Enfermedades Raras?
*/
get_header(); 
include('sub-header.php');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div id="que-son-las-er" class="feed">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content-que_son_las_er', get_post_format() );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
