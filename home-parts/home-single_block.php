<?php

//Get all the values of the 'section'
$title = get_sub_field( 'title-single_block' );
$title_color = get_sub_field( 'title_color' );
$text = get_sub_field( 'text-single_block' );
$text_color = get_sub_field( 'text_color' );
$size = get_sub_field( 'block_size-single_block' );
$select_image = get_sub_field( 'select-image-single_block' );
$get_image = get_sub_field( 'image-single_block' );
$post_image = wp_get_attachment_image_src( $get_image, 'full' );
$select_circle_image = get_sub_field( 'select-circle-image-single_block' );
$alignment_content = get_sub_field( 'select-alignment-image-single_block' );
$select_background = get_sub_field( 'select-background-single_block' );
$image_background = get_sub_field( 'background_image-single_block');
$color_mask = get_sub_field( 'color_mask' );
$background_image = wp_get_attachment_image_src( $image_background, 'full' );
$select_background_image = get_sub_field( 'select-fixed-background_image' );
$background_color = get_sub_field( 'background_color-single_block' );
$select_add_button = get_sub_field( 'select-add_button' );
$text_button = get_sub_field( 'text-button' );
$link_button = get_sub_field( 'link-button' );
$color_button = get_sub_field( 'color-button' );

//Conditional: size has value, else default size 'medium'.
	if( $size != null ) {
		$size = get_sub_field( 'block_size-single_block' );
	} else {
		$size = 'medium';
	}

//Conditional: background is and image or a color.
	if( $select_background != null ) {

		if ( $select_background == 'white' ) {
				$background = 'background-color: white';

		} elseif ( $select_background == 'color' ) {

				if( $background_color != null ) {
					$background = 'background-color: ' . $background_color;
				} else {
					$background = 'background-color: white';
				}

		} elseif ( $select_background == 'image' ) {
				
				if( $select_background_image != null ) {

					if( $select_background_image == 'true' ) {
							$background = '
							background-image: url(' . $background_image[0] . '); 
					    background-repeat: no-repeat;
					    background-attachment: fixed;
					    background-size: cover;
					    background-position: center;
					    ';
					} elseif( $select_background_image == 'false') {
							$background = '
							background-image: url(' . $background_image[0] . '); 
					    background-repeat: no-repeat;
					    background-size: cover;
					    background-position: center;
					    ';
					} else {
						$background = '
							background-image: url(' . $background_image[0] . '); 
					    background-repeat: no-repeat;
					    background-size: cover;
					    background-position: center;
					    ';
					}
				} else {
						$background = '
							background-image: url(' . $background_image[0] . '); 
					    background-repeat: no-repeat;
					    background-size: cover;
					    background-position: center;
					    ';
				}

		} else {
			$background = 'background-color: white';
		}
	}

	//Conditional: want a post image and if have it.
		if( $select_image != null ) {
			if( $select_image == 'true' ) {
				$inserting_image = '<img id="post-image" src="' . $post_image[0] . '">';
				$proportion = 'middle';
				$size_whitout_image_container = '';
				$size_whitout_image_section = '';
			} elseif ( $select_image == 'false' ) {
				$inserting_image = '';
				$proportion = '';
				$size_whitout_image_container = 'unique';
				$size_whitout_image_section = 'unique-container';
			} else {
				$proportion = '';
				$size_whitout_image_container = 'unique';
				$size_whitout_image_section = 'unique-container';
			}
		}

	//Conditional: want a button and if have it.
		if( $select_add_button != null ) {
			if( $select_add_button == 'true' ) {
				$inserting_button = 
				'<a id="post-button" href="' . $link_button . '" style="background-color: ' . $color_button . '">'
						. __($text_button, 'apt') .
					'</a>';
			} elseif ( $select_image == 'false' ) {
				$inserting_button = '';
			} else {
				$inserting_button = '';
			}
		}

	//Conditional: aligning content, by default is left.
		if( $alignment_content != null ) {
			if( $select_image == 'true' ){
				$alignment_content = get_sub_field( 'select-alignment-image-single_block' );
			} else {
				$alignment_content = '';
			}
		} else {
			$alignment_content = 'left';
		}

	//Conditional: getting the button color, by default 'orange'.
		if( $color_button != null ) {
			$color_button = get_sub_field( 'color-button' );
		} else {
			$color_button = '#ff6a00';
		}

	//Conditional: the post image it is circle or no.
		if( $select_circle_image != null ) {
			if( $select_image == 'true' ) {
				if( $select_circle_image == 'true') {
					$circle_image = 'circle-image';	
				} else {
					$circle_image = '';
				}
			}
		} else {
			$circle_image = '';
		}

/*
*Displaying Content 
*/
?>
	
<div class="section size-<?= $size ?> <?= $size_whitout_image_section ?>" style=" <?= $background ?>">
		
		<div class="container alignment-<?= $alignment_content?> <?= $size_whitout_image_container ?>">
			<div class="container-image <?= $proportion ?> <?= $circle_image ?>">
				<?= $inserting_image ?>
			</div>
			<div class="container-text <?= $proportion ?>">
				<h2 id="post-title" style="color: <?= $title_color ?>"><?= __($title, 'apt') ?></h2>
				<p class="text_color-<?= $text_color ?>" id="post-text"><?= __($text, 'apt') ?></p>
				 <?= $inserting_button ?>
			</div>
		</div>

</div>