<?php

//Getting default values
$size_carrousel = get_sub_field( 'size_carrousel' );
$select_add_navigation = get_sub_field( 'select-add_navigation' );

?>
<div class="section carrousel">
    <div class="swiper-container carrousel size-<?= $size_carrousel ?>">
        <div class="swiper-wrapper">

<?php
//Getting all the slides 
while( have_rows('slides') ): the_row(); 

//Values of the slide
    $get_image = get_sub_field('image');
    $image = wp_get_attachment_image_src( $get_image, 'full' );
    $select_image_fixed = get_sub_field( 'select-image_fixed' );
    $select_add_text = get_sub_field( 'select-add_text' );
    $title = get_sub_field( 'title_carrousel' );
    $title_color = get_sub_field( 'title_color' );
    $text = get_sub_field( 'text_carrousel' );
    $text_color = get_sub_field( 'text_color' );
    $select_add_button = get_sub_field( 'select-add_button' );
    $text_button = get_sub_field( 'text_button' );
    $link_button = get_sub_field( 'link_button' );
    $color_button = get_sub_field( 'color_button' );


//Conditional: add navigation, default 'no'.
    if( $select_add_navigation != null ) {
        if ( $select_add_navigation == 'true' ) {
            $navigation = '
                <a class="swiper-button-prev">
                    <i class="i-arrow-left"></i>
                </a>
                <a class="swiper-button-next">
                    <i class="i-arrow-right"></i>
                </a>
            ';
        } else {
            $navigation = '';
        }
    } else {
        $navigation = '';
    }

//Conditional: background image
    if ( $image != null ) {
        if ( $select_image_fixed == 'true') {
            $background = '
            background: url('. $image[0] .') no-repeat;
            background-size: cover;
            background-attachment: fixed;
            background-position: center;
            ';
        } else {
            $background = '
            background: url('. $image[0] .') no-repeat;
            background-size: cover;
            background-position: center;
            ';
        }
        
    }

//Conditional: want a button and if have it.
    if( $select_add_button != null ) {
        if( $select_add_button == 'true' ) {
            $inserting_button = 
            '<a id="post-button" href="' . $link_button . '" style="background-color: ' . $color_button . '">'
                    . __($text_button, 'apt') .
                '</a>';
        } else {
            $inserting_button = '';
        } 
    } else {
        $inserting_button = '';
    }

//Conditional: add text or not
    if( $select_add_text != null ) {
        if ( $select_add_text == 'true' ) {
            $text_area = '
            <div class="container-text">
                <h2 id="post-title" style="color:' . $title_color . '">'. __($title, 'apt') .'</h2>
                <p class="text_color-'. $text_color .'" id="post-text">'. __($text, 'apt') .'</p>'
                . $inserting_button . '
            </div>
            ';
        } else {
            $text_area = '';
        }
    } else {
        $text_area = '';
    }


/*
* Displaying content
*/
?>

    <div class="swiper-slide" style="<?= $background ?>">
        
        <div class="container unique">
            <?= $text_area ?>
        </div>

    </div>

<?php endwhile; ?>

    </div>

        <?= $navigation ?>

    </div>
</div>