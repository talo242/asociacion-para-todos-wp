<div class="cards-section">
	<div class="container-cards">
<?php

while( has_sub_field('card_content') ):

//Get all the values of the 'card'
$title = get_sub_field( 'title-card' );
$title_color = get_sub_field( 'title_color' );
$text = get_sub_field( 'text-card' );
$text_color = get_sub_field( 'text_color' );
$select_image_card = get_sub_field( 'select-image-card' );
$select_circle_image = get_sub_field( 'select-circle-image-card' );
$get_image = get_sub_field( 'image-card' );
$image_card = wp_get_attachment_image_src( $get_image, 'xp-index-post-size' );
$select_background = get_sub_field( 'select-background' );
$background_color = get_sub_field( 'background-color' );
$border_color = get_sub_field( 'border_color' );
$select_add_button = get_sub_field( 'select-add_button' );
$text_button = get_sub_field( 'text-button' );
$link_button = get_sub_field( 'link-button' );
$color_button = get_sub_field( 'color-button' );
$inserting_image = '';
$inserting_button = '';

//Conditional: want a post image and if have it.
	if( $select_image_card != null ) {
		if( $select_image_card == 'true' ) {
			$inserting_image = '<img id="image-card" src="' . $image_card[0] . '">';
		} elseif ( $select_image_card == 'false' ) {
			$inserting_image = '';
		}
	}

//Conditional: want a button and if have it.
	if( $select_add_button != null ) {
		if( $select_add_button == 'true' ) {
			$inserting_button = 
			'<a id="card-button" href="' . $link_button . '" style="background-color: ' . $color_button . '">'
					. __($text_button, 'apt') .
				'</a>';
		} elseif ( $select_add_button == 'false' ) {
			$inserting_button = '';
		} else {
			$inserting_button = '';
		}
	}

//Conditional: the post image it is circle or no.
	if( $select_circle_image != null ) {
		if( $select_circle_image == 'true') {
			$circle_image = 'circle-image';	
		} else {
			$circle_image = '';
		}
	} else {
		$circle_image = '';
	}

/*
* Displaying content
*/
?>

<div class="card-content" style="border: 2px solid <?= $border_color ?>">
	<div class="card-image <?= $circle_image ?>">
		<?= $inserting_image ?>
	</div>
	<div class="card-text">
		<h2 id="post-title" style="color: <?= $title_color ?>"><?= __($title, 'apt') ?></h2>
		<p class="text_color-<?= $text_color ?>" id="post-text"><?= __($text, 'apt') ?></p>
		<?= $inserting_button ?>
	</div>
</div>

<?php endwhile; ?>
	</div>
</div>