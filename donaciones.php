<?php
/*
*
* Template Name: Donaciones
*
* This template is for 'Donaciones' page.
*
*/
get_header();
include('sub-header.php');

if( have_rows('cards_donation') ):
?>

<div class="site-main donaciones">

<div id="title-page">
	<h2>Puedes ayudar a los niños de la 
	Asociación de las siguientes maneras:</h2>	
</div>


<?php 

  while ( have_rows('cards_donation') ) : the_row();
		if( get_row_layout() == 'cards_donation-content' ):

		$get_icon = get_sub_field( 'icon' );
		$icon = wp_get_attachment_image_src( $get_icon, 'xp-index-post-size' );
		$title = get_sub_field( 'title' );
		$text = get_sub_field( 'text' );
		$select_add_button = get_sub_field( 'select-add_button' );
		$text_button = get_sub_field( 'text_button' );
		$link_button = get_sub_field( 'link_button' );


//Conditional: have an icon.
		if( $icon != null ) {
			$icon_area = '<img src="'. $icon[0] .'">';
		} else {
			$icon_area = '';
		}

	//Conditional: add a button.
		if( $select_add_button == 'true' ) {
			$add_button = '<a id="button" href="'.$link_button.'">'.__( $text_button ,'apt').'</a>';
		} else {
			$add_button = '';
		}

/*
* Displaying content
*/
?>

		<div class="card-donation">
			<div id="icon-area">
				<?= $icon_area ?>
			</div>
			<div id="text-area">
				<h3><?= $title ?></h3>
				<p><?= $text ?></p>
			</div>
			<div id="button-area">
				<?= $add_button ?>
			</div>
		</div>

<?php 
			endif;
		endwhile;
  endif;
  ?>

</div>

 <?php
get_footer();