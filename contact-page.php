<?php
/*
* Template Name: Tarjetas 
*/

get_header();

$image = get_field('contact_page_sub-header_thumbnail', 'option');
$color = get_field('contact_page_sub-header_color', 'option');

$telefono = get_field('telefono', 'option');
$direccion =get_field('direccion', 'option');
$email = get_field('email', 'option');

?>

<div class="title-info-contact">
	<h3>¿Tienes alguna duda?</h3>
	<h4>Ponte en contácto con la Asociación.</h4>
</div>

<div class="sub-header-contact-page" style="background-color:<?= $color ?>">
	
		<div class="sub-header-image-container-contact-page">

				<div class="sub-header-image-contact-page" 
					style="
						background: url(<?= $image['sizes']['xp-single-header-size']; ?>) no-repeat;
						background-size: cover;
					">	
				</div>
			
		</div>

		<div id="page-title">
				<h2><?= apt_title(); ?></h2>	
		</div>

</div>


<div class="contact-page">
	
	<div class="content-form">
			<div class="form-contact-page">
				
				<div id="text-area">
					<div class="identity-area-contact">
						<input id="name" type="name" placeholder="Nombre completo">
						<input id="email" type="email" placeholder="Dirección de correo electrónico">
					</div>
					<div class="message-area-contact">
						<textarea id="message" type="message" placeholder="Escribir mensaje"></textarea>
					</div>
				</div>

				<div class="button-area-contact">
					<button id="button" type="submit" name="enviar">ENVIAR</button>
				</div>

			</div>
	</div>

	<div class="content-info">
		<div class="info-contact">
			<p><i class="i-phone"></i><a href="tel:<?= $telefono ?>"><?= $telefono ?></a></p>
		  <p><i class="i-location"></i><a><?= $direccion ?></a></p>
		  <p><i class="i-envelop"></i><a href="mailto:<?= $email ?>"><?= $email ?></a></p>
		</div>
	</div>
	
</div>


<?php
get_footer();