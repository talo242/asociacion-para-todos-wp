<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package asociacion-para-todos
 */
get_header(); 
include('sub-header.php'); 
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main single-enfrar">

		<div class="single-content">
			<div class="feed single-xp">

				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content-single-apt_enf_rar', get_post_type() );
				endwhile; // End of the loop.
				?>
				
			</div>
		</div>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
