<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */
$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id($post), 'noticias-index-size' );

?>
<article id="post-<?php the_ID();  ?>" class="post-page" >
		<div class="post-image" >
			<img src="<?php echo $featured_img[0] ?>">
		</div>
		<div class="post-title-info">
			<?php
				//--Fecha--
				if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta date-index">
						<?php asociacion_para_todos_posted_on_month_day(); ?>
					</div><!-- .entry-meta -->
					<?php
				endif;
			?>
			<div class="post-title">
				<a class="post-title-link" href="<?php asociacion_para_todos_post_link(); ?>" rel="bookmark">
					<h2>
						<?php 
							echo get_post_field( 'post_title', get_post() );
						?>
					</h2>
				</a>
			</div>
		</div>
	
	<div class="post-content">
			<?php
				the_excerpt( apt_excerpt_more() );
			?>
		</div>

	<div class="footer-post">
		<?php asociacion_para_todos_entry_footer_autor_fecha_index(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->