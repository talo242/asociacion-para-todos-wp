<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */
?>

<article id="post-xp-<?php the_ID();  ?>" class="post-page single-post-xp" >

	<div class="post-title single">
			<h2>
				<?php 
					echo get_post_field( 'post_title', get_post() );
				?>
			</h2>
	</div>

	<div class="footer-post-xp-single">
		<?php asociacion_para_todos_entry_footer_autor_fecha_xp(); ?>
	</div>
	
	<div class="post-content">
			<?php
				the_content();
			?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
