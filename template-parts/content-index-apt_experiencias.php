<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */
$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id($post), 'xp-index-post-size' );
?>
<article id="xp-<?php the_ID();  ?>" class="xp-page" >
		<div class="xp-image" >
			<img class="wp-post-image" src="<?php echo $featured_img[0]; ?>">
		</div>
		<div class="">
			<?php
				//--Fecha--
				if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta date-index">
						<?php asociacion_para_todos_posted_on_month_day(); ?>
					</div><!-- .entry-meta -->
					<?php
				endif;
			?>
			<div class="experiencias-text">
				<a class="post-title-link" href="<?php asociacion_para_todos_post_link(); ?>" rel="bookmark">
					<h2>
						<?php 
							echo get_post_field( 'post_title', get_post() );
						?>
					</h2>
				</a>
			</div>
		</div>
	
	<div class="experiencias-content">
			<?php
				the_excerpt( );
			?>
		</div>

	<div class="experiencias-footer-article">
		<?php asociacion_para_todos_entry_footer_autor_fecha_xp(); ?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->