<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */


 while ( have_rows( 'informative_page' ) ) : the_row();

 	if( get_row_layout() == 'block-informative_page' ):

 	//Getting all values
 	$title = get_sub_field( 'title' );
 	$title_color = get_sub_field('title_color');
 	$text = get_sub_field( 'text' );
 	$content_alignment = get_sub_field( 'content_alignment' );

?>

		<div class="informative-page-block">
			
			<div class="content-area block-align-<?= $content_alignment ?>">

				<div id="title-area">
					<h2 style="color: <?= $title_color ?>"><?= $title ?></h2>	
				</div>
				
				<div id="text-area">
					<p><?= $text ?></p>
					<?php

					if ( have_rows( 'extras' ) ):
				 		while ( have_rows( 'extras' ) ) : the_row();

							if( get_row_layout() == 'add_quote' ):
					 			$quote = get_sub_field('quote');

					 			if( $quote != null ) {
					 				?>
					 					<div id="quote-area">
											<p><?= $quote ?></p>
										</div>
									<?php
					 			}

					 		endif;
					 	endwhile;
					endif;

					?>
				</div>
				
				<?php
				if ( have_rows( 'extras' ) ):
			 		while ( have_rows( 'extras' ) ) : the_row();

			 			if( get_row_layout() == 'add_image' ):

			 				$get_image = get_sub_field( 'image' );
							$image = wp_get_attachment_image_src( $get_image, 'full' );

							if( $image != null ) {
									?>
									<div id="image-area">
										<img src="<?= $image[0] ?>">
									</div>
									<?php
							}

			 				endif;

					endwhile;
				endif;
 				?>

 				<div class="button-area block-align-<?= $content_alignment ?>">
					 <?php 
					 	if ( have_rows( 'extras' ) ):
					 		while ( have_rows( 'extras' ) ) : the_row();

					 			if( get_row_layout() == 'add_button' ):
					 				$text_button = get_sub_field('text_button');
					 				$link_button = get_sub_field('link_button');
					 				$color_button = get_sub_field('color_button');
					 				$text_color_button = get_sub_field('text_color');			 				

						 			if( $text_button != null ) {
						 			?>					
									
						 				<a href="<?= $link_button ?>" style="background-color: <?= $color_button ?>; 
						 				color: <?= $text_color_button ?>">
						 					<?= __($text_button,'apt') ?>
						 				</a>

							 		<?php

						 			}
					 			endif;

					 		endwhile;
					 	endif;
					 ?>
				</div>

				</div>
				
		</div>

<?php
		endif;
	endwhile;
?>