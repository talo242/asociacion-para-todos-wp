<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */
?>

<article id="enfermedad_rara-<?php the_ID();  ?>" class="post-page " >
	<div class="contenido_enfer_rara">

		<div id="descripcion-patologia">
				<h2><?php _e( 'Descripción ', 'apt') ?> </h2>
				<?php the_content(); ?>
		</div>

		<?php
			if( have_rows('contenido_enfer_rara') ):

		    while ( have_rows('contenido_enfer_rara') ) : the_row();

		//CAUSAS
		        if( get_row_layout() == 'causas' ):
		        	//Llamando valores abreviados
		        	$imagen_causas = wp_get_attachment_image_src( get_sub_field('imagen_causas'), 'xp-index-post-size' );

		        	?> 
							
							<div id="causas">
								<h2 id="title">Causas </h2>
								<div id="texto">									
				        	<h4 id="titulo"> <?= the_sub_field('titulo_causas'); ?> </h4>
				        	<p id="descripcion"> <?= the_sub_field('descripcion_causas'); ?> </p>
								</div>
			        	<img id="imagen" src="<?= $imagen_causas[0]; ?>">
							</div>	
		        	
		        	<?php

		//SINTOMAS
		        elseif( get_row_layout() == 'sintomas' ):
		        	//Llamando valores abreviados
		        	$imagen_sintomas = wp_get_attachment_image_src( get_sub_field('imagen_sintomas'), 'xp-index-post-size' );

		        	?> 

							<div id="sintomas">
								<h2 id="title">Síntomas </h2>
								<img id="imagen" src="<?= $imagen_sintomas[0]; ?>">
								<div id="texto">									
				        	<p id="descripcion"> <?= the_sub_field('descripcion_imagen_sintomas'); ?> </p>
								</div>
							</div>	
		        	
		        	<?php
		
		//TRATAMIENTOS
		        elseif( get_row_layout() == 'tratamiento' ):
		        	//Llamando valores abreviados
		        	$imagen_tratamiento = wp_get_attachment_image_src( get_sub_field('imagen_tratamiento'), 'xp-index-post-size' );

		        	?> 

							<div id="tratamiento">
								<h2 id="title">Tratamientos </h2>
								<div id="texto">									
				        	<p id="descripcion"> <?= the_sub_field('descripcion_imagen_tratamiento'); ?> </p>
								</div>
								<img id="imagen" src="<?= $imagen_tratamiento[0]; ?>">
							</div>	
		        	
		        	<?php

		 //PATOLOGIAS
		        elseif( get_row_layout() == 'patologias' ):
		        	//Llamando valores abreviados
		        	$imagen_patologia = wp_get_attachment_image_src( get_sub_field('imagen_patologia'), 'xp-index-post-size' );

		        	?> 

							<div id="patologias">
								<h2 id="title">Patologías </h2>
								<img id="imagen" src="<?= $imagen_patologia[0]; ?>">
								<div id="texto">		
				        	<p id="patologias"> <?= the_sub_field('patologias-patologias'); ?> </p>
				        	<p id="tipos"> <?= the_sub_field('tipo_de_patologia-patologia'); ?> </p>
								</div>
							</div>	
		        	
		        	<?php


		        endif;

		    endwhile;
			endif;
		?>

	</div>

	<div class="footer-post-xp-single">
		<?php asociacion_para_todos_entry_footer_autor_fecha_xp(); ?>
	</div>
	
</article><!-- #post-<?php the_ID(); ?> -->
