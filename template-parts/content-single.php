<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */
?>
<article id="post-<?php the_ID();  ?>" class="post-page" >
	<div class="post-title single">
			<h2>
				<?php 
					echo get_post_field( 'post_title', get_post() );
				?>
			</h2>
	</div>
	<div class="footer-post">
	<?php asociacion_para_todos_entry_footer_autor_fecha_single(); ?>
	</div>
	
	<div class="post-image" >
		<?php the_post_thumbnail( 'post-size' ) ?>
	</div>
	<div class="post-content">
			<?php
				the_content();
			?>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
