<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */
?>
<article id="enfrar-<?php the_ID();  ?>" class="enfrar-page" >
	<div class="enfrar-text">
		<a class="post-title-link" href="<?php asociacion_para_todos_post_link(); ?>" rel="bookmark">
			<h2>
				<?php 
					echo get_post_field( 'post_title', get_post() );
				?>
			</h2>
		</a>
	</div>
	
	<div class="enfrar-content">
			<?php
				the_excerpt( );
			?>
		</div>
</article><!-- #post-<?php the_ID(); ?> -->