<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package asociacion-para-todos
 */

////////////////////////////////////////////////

if ( ! function_exists( 'asociacion_para_todos_post_link' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function asociacion_para_todos_post_link() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( '') ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'asociacion-para-todos' ),
			//It is removed: [29:17] - Posted on 
			 esc_url( get_permalink() )
		);

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'asociacion-para-todos' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo $posted_on; // WPCS: XSS OK.
		//It is removed: [39:58] - <span class="byline"> ' . $byline . '</span>

	}
endif;

////////////////////////////////////

if ( ! function_exists( 'asociacion_para_todos_posted_on_month_day' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function asociacion_para_todos_posted_on_month_day() {
		$time_string1 = '<a class="entry-date-published-month">%2$s</a>';
		$time_string2 = '<a class="entry-date-published-day">%2$s</a>';

		$time_string1 = sprintf( $time_string1,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( 'M') )
		);

		$time_string2 = sprintf( $time_string2,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( 'd') )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'asociacion-para-todos' ),
			//It is removed: [29:17] - Posted on 
			'<p>' . $time_string1 .  $time_string2 . '</p>'
		);

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'asociacion-para-todos' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<div class="posted-on">' . $posted_on . '</div>'; // WPCS: XSS OK.
		//It is removed: [39:58] - <span class="byline"> ' . $byline . '</span>

	}
endif;

///////////////////////////////////////////////

if ( ! function_exists( 'asociacion_para_todos_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function asociacion_para_todos_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( '') ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'asociacion-para-todos' ),
			//It is removed: [29:17] - Posted on 
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'asociacion-para-todos' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.
		//It is removed: [39:58] - <span class="byline"> ' . $byline . '</span>

	}
endif;

/////////////////////////////////////

if ( ! function_exists( 'asociacion_para_todos_entry_footer_autor_fecha_index' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function asociacion_para_todos_entry_footer_autor_fecha_index() {
		
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'Por: %s', 'post author', 'asociacion-para-todos' ),
			'<span class="author vcard">' . esc_html( get_the_author() ) . '</span>'
		);

		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( 'd/m/y') ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Publicado el: %s', 'post date', 'asociacion-para-todos' ),
			//It is removed: [29:17] - Posted on 
			$time_string
		);

		echo '<span class="author-post">' . $byline . '</span>';
		echo '<span class="date-post">' . $posted_on . '</span>';

	}
endif;

/////////////////////////////////////

if ( ! function_exists( 'asociacion_para_todos_entry_footer_autor_fecha_single' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function asociacion_para_todos_entry_footer_autor_fecha_single() {

		$categories_list = get_the_category_list( esc_html__( ', ', 'asociacion-para-todos' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">En: ' . esc_html__( '%1$s', 'asociacion-para-todos' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'Por: %s', 'post author', 'asociacion-para-todos' ),
			'<span class="author vcard">' . esc_html( get_the_author() ) . '</span>'
		);

		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( 'd/m/y') ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Publicado el: %s', 'post date', 'asociacion-para-todos' ),
			//It is removed: [29:17] - Posted on 
			$time_string
		);

		echo '<div class=author-date>';
			echo '<span class="author-post">' . $byline . '</span>';
			echo '<span class="date-post">' . $posted_on . '</span>';
		echo '</div>';
	}
endif;

/////////////////////////////////////

if ( ! function_exists( 'asociacion_para_todos_entry_footer_autor_fecha_xp' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function asociacion_para_todos_entry_footer_autor_fecha_xp() {

		$categories_list = get_the_category_list( esc_html__( ', ', 'asociacion-para-todos' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">En: ' . esc_html__( '%1$s', 'asociacion-para-todos' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'Por: %s', 'post author', 'asociacion-para-todos' ),
			'<span class="author vcard">' . esc_html( get_the_author() ) . '</span>'
		);

		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( 'd/m/y') ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Fecha: %s', 'post date', 'asociacion-para-todos' ),
			//It is removed: [29:17] - Posted on 
			$time_string
		);

		echo '<div class=xp-author-date>';
			echo '<span class="author-post">' . $byline . '</span>';
			echo '<span class="date-post">' . $posted_on . '</span>';
		echo '</div>';
	}
endif;

/////////////////////////////////////

if ( ! function_exists( 'asociacion_para_todos_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function asociacion_para_todos_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'asociacion-para-todos' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( '%1$s', 'asociacion-para-todos' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'asociacion-para-todos' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'asociacion-para-todos' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'asociacion-para-todos' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'asociacion-para-todos' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;
