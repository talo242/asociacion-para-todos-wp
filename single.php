<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package asociacion-para-todos
 */

get_header(); 
include('sub-header.php');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="feed noticias">

				<button class="sidebar-button" aria-controls="widget-area" aria-expanded="false">
					<i class="i-arrow-down"></i>
					<?php esc_html_e( ' Ver categorías', 'widget-area' ); ?>
				</button>
				<div class="sidebar-feed">
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				</div>

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content-single', get_post_type() );


					// If comments are open or we have at least one comment, load up the comment template.
					//if ( comments_open() || get_comments_number() ) :
						//comments_template();
					//endif;

				endwhile; // End of the loop.
				?>

				</div>

				<div class="sidebar-area-right">
					<?php get_sidebar(); ?>		
				</div>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php
get_footer();
