<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */

get_header();
include('sub-header.php');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="feed noticias">

				<?php
				if ( have_posts() ) :

					/* Start the Loop */
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content-index', get_post_format() ); 	
					endwhile; 
				?>

					<div class="post-navigation">
						<?php
							the_posts_pagination( array( 
							"mid_size" => 3,
							"prev_text" => "<i class=\"i-arrow-left\"></i>",
							"next_text" => "<i class=\"i-arrow-right\"></i>",
							) );
						?>
					</div>
					
				<?php endif; ?>

			</div>	

			<div class="sidebar-area-right xp-sidebar">
				<div>
					<h2 class="widget-title servicios-text">Otros Servicios </h2>
					
					<?php
						//listado de taxonomias
						$taxonomy = 'apt_servicios_categorias';
						$tax_terms = get_terms($taxonomy);
						?>
						<ul>
						<?php
						foreach ($tax_terms as $tax_term) {
							$link = esc_attr(get_term_link($tax_term, $taxonomy));
							echo '<li><a href="' .$link. '">' .$tax_term->name. '</a></li>';
						}
					?>
						</ul>

				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
