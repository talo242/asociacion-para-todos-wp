<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package asociacion-para-todos
 */
$featured_img = wp_get_attachment_url( get_post_thumbnail_id(), 'xp-single-header-size' );
get_header(); 
include('sub-header.php');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main single-xp">
		
		<?php if ($featured_img != null) : ?>
			<div class="single-experiencias-image" style="background-image: url('<?= $featured_img ?>')">
			</div>
		<?php endif ?>

		<?=
			$categories_list = get_the_category_list( esc_html__( ', ', 'asociacion-para-todos' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( '%1$s', 'asociacion-para-todos' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}
		?>
		
		<div class="single-content">

			<div class="feed single-xp">

				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content-single-apt_experiencias', get_post_type() );
				endwhile; // End of the loop.
				?>

				</div>

			</div>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php
get_footer();
