<?php
/*
*
* Template Name: Tarjetas Responsive
*
*/
get_header();
include('sub-header.php');


if( have_rows('cards_template') ):
?>

	<div id="primary" class="content-area xp-content">
		<main id="main" class="site-main xp-site">
			<div class="feed feed-historias">

<?php 

  while ( have_rows('cards_template') ) : the_row();

		$title = get_sub_field( 'title-card_template' );
		$content = get_sub_field( 'content-card_template' );
		$get_image = get_sub_field( 'image-card_template' );
		$image = wp_get_attachment_image_src( $get_image, 'xp-index-post-size' );
		$select_add_email = get_sub_field('select_add_email-card_template');
		$select_add_phone = get_sub_field('select_add_phone-card_template');


		if ( $select_add_email == 'true' ) {
			$email = 'Email: ' . get_sub_field('email-card_template');
		} else {
			$email = '';
		}

		if ( $select_add_phone == 'true' ) {
			$phone = 'Tel: ' . get_sub_field('phone_number-card_template');
		} else {
			$phone = '';
		}
?>

<article class="xp-page" >
		
		<div class="xp-image" >
			<img class="wp-post-image" src="<?= $image[0] ?>">
		</div>
		
		<div class="experiencias-text">
			<h2>
				<?= $title ?>
			</h2>
		</div>
	
	<div class="experiencias-content">
		<p><?= $content ?></p>
	</div>

	<div class="experiencias-footer-article">
		<div class="xp-author-date">
				<span class="author-post">
					<?= $email ?>
				</span>
				<span class="date-post">
					<?= $phone ?>
				</span>
		</div>
	</div>

</article>

<?php

	endwhile;
endif;

get_footer();