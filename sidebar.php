<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package asociacion-para-todos
 */

if ( ! is_active_sidebar( 'sidebar-1', 'sidebar-2' ) ) {
	return;	
}
?>

<aside id="secondary" class="widget-area">
	<div>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</div>
	<div class="entradas-populares">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div>
</aside><!-- #secondary -->
