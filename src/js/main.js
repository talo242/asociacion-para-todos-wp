/**
* royalestudios.com
*
* Copyright (c) 2017-07-17 Ricardo Velasco
* Licensed under the GPLv2+ license.
*/

// You can use ECMAScript 2015 modules
// Ex. import module from 'module-name';

( function( $ ) {

//Animando el navbar
  var previousScroll = 0,
  headerOrgOffset = $('.navbar-animated').offset().top + 80;

  $(window).scroll(function() {
      var currentScroll = $(this).scrollTop();
      if(currentScroll > headerOrgOffset) {
          if (currentScroll > previousScroll) {
              $('.navbar-menu').addClass('hidden');
              $('.navbar-animated').addClass('hidden');
          } else {
              $('.navbar-menu').removeClass('hidden');
              $('.navbar-animated').removeClass('hidden');
          }
      } else {
          $('.navbar-menu').removeClass('hidden');
          $('.navbar-animated').removeClass('hidden');  
      }
      previousScroll = currentScroll;
  });

  //Animando el sub-header
    setTimeout(function(){ 
      $(".wp-sub-header-image").addClass('show-wp-sub-header-image');
     }, 1000);

//Desabilitar el scroll
  // left: 37, up: 38, right: 39, down: 40,
  // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
  var keys = [37, 38, 39, 40];

  function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;  
  }

  function keydown(e) {
      for (var i = keys.length; i--;) {
          if (e.keyCode === keys[i]) {
              preventDefault(e);
              return;
          }
      }
  }

  function DoPrevent(e) {
    preventDefault(e);
  }

  function disable_scroll() {

    if (window.addEventListener) {
        window.addEventListener('DOMMouseScroll', DoPrevent, false);
    }
    window.onmousewheel = document.onmousewheel = DoPrevent;
    document.onkeydown = keydown;
    $('body').on('touchmove', DoPrevent);
    $('.navbar-links').off('touchmove', DoPrevent);
    
  }

  function enable_scroll() {
      if (window.removeEventListener) {
          window.removeEventListener('DOMMouseScroll', DoPrevent, false);
      }
      window.onmousewheel = document.onmousewheel = document.onkeydown = null;
      $('body').off('touchmove', DoPrevent);
  }

// Mostrar menú en versión móvil
  $('.menu-toggle').click(function() {

    $(".site-navigation").toggleClass('active-navbar');
    $(".all-content").toggleClass('slow-body');

    if( $(".site-navigation").hasClass('active-navbar') ){
      disable_scroll();
      $(".menu-toggle").addClass('toggle-off');
    } else {
      enable_scroll();
      $(".menu-item").children(".sub-menu").removeClass("sub-menu-active");
      $(".menu-toggle").removeClass('toggle-off');
    }

  });

  $(".all-content").click(function() {
      $(".menu-toggle").removeClass('toggle-off');
      $(".menu-item").children(".sub-menu").removeClass("sub-menu-active");
      $(".site-navigation").removeClass('active-navbar');
      $(".all-content").removeClass('slow-body');
      enable_scroll();
  });

// Mostrar submenús al hacer click
  $(".menu-item").click(function() {
      const showContent = $(this).children(".sub-menu").hasClass("sub-menu-active");
      if (showContent == false){
        $(".sub-menu").removeClass("sub-menu-active");
        $(this).children(".sub-menu").toggleClass("sub-menu-active");
      } else {
        $(this).children(".sub-menu").addClass("sub-menu");
        $(this).children(".sub-menu").removeClass("sub-menu-active");
      }
  });

// Mostrar menu del footer.
  const inf = document.querySelector('.informate-button');
  const part = document.querySelector('.participa-button');
  const apoy = document.querySelector('.apoya-button');
  const cont = document.querySelector('.contactanos-button');

  inf.addEventListener('click', function() {
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = `${panel.scrollHeight}px`;
    } 
  });
  part.addEventListener('click', function() {
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = `${panel.scrollHeight}px`;
    } 
  });
  apoy.addEventListener('click', function() {
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = `${panel.scrollHeight}px`;
    } 
  });
  cont.addEventListener('click', function() {
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = `${panel.scrollHeight}px`;
    } 
  });

//Swipper 
  const mySwiper = new Swiper ('.swiper-container', {
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      autoplay: 5000,
      autoplayDisableOnInteraction: false,
    });

} )( jQuery );