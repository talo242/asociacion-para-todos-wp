( function( $ ) {

// Mostrar sidebar dentro del feed
  const sb = document.querySelector('.sidebar-button');

  sb.addEventListener('click', function() {
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = `${panel.scrollHeight}px`;
    } 
  });

 } )( jQuery );