<?php
/**
 *Template Name: Lista de Enfermedades Raras
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package asociacion-para-todos
 */

get_header();
include('sub-header.php');
?>

	<div id="primary" class="content-area xp-content">
		<main id="main" class="site-main xp-site">
			<div class="feed feed-er">

				<?php
				if ( have_posts() ) :

					if ( is_home() && ! is_front_page() ) : ?>
						<header>
							<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
						</header>

					<?php
					endif;

					/* Start the Loop */
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content-index-apt_enf_rar', get_post_format() ); 	
					endwhile; 
				
				endif; ?>

			</div>	

			<div class="post-navigation xp-nav">
				<?php
					the_posts_pagination( array( 
					"mid_size" => 3,
					"prev_text" => "<i class=\"i-arrow-left\"></i>",
					"next_text" => "<i class=\"i-arrow-right\"></i>",
					) );
				?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();