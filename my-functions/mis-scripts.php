<?php

function mis_scripts() {

	if( is_archive('apt_experiencias') ) {

			wp_register_script( 'masonry-scripts', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', ['jquery'], null, true );

			wp_enqueue_script( 'masonry-scripts');

			wp_register_script('experiencias-scripts', get_template_directory_uri() . '/dist' . mix('/js/experiencias-scripts.js'), ['jquery'], null, true);

			wp_enqueue_script( 'experiencias-scripts');

	} elseif ( is_page_template('contact-page.php') ) {

			wp_register_script('contact_page', get_template_directory_uri() . '/dist' . mix('/js/contact_page.js'), ['jquery'], null, true);

			wp_enqueue_script( 'contact_page');	

	} elseif ( is_home() || is_single() ) {

			wp_register_script('noticias-scripts', get_template_directory_uri() . '/dist' . mix('/js/noticias-scripts.js'), ['jquery'], null, true);

			wp_enqueue_script( 'noticias-scripts');	

	}

}

add_action( 'wp_enqueue_scripts', 'mis_scripts' );
