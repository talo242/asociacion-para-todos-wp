<?php

//Agregando los Options Page - ACF

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' => 'Sub-Header',
		'menu_title' => 'Sub-Header',
		'menu-slug' => 'sub-header',
		'capability' => 'edit_posts',
		'position' => 51,
		'icon-url' => false,
		'redirect' => false,
	));

	acf_add_options_page(array(
		'page_title' => 'Contácto',
		'menu_title' => 'Contácto',
		'menu-slug' => 'contacto',
		'capability' => 'edit_posts',
		'position' => 50,
		'icon-url' => false,
		'redirect' => false,
	));

}