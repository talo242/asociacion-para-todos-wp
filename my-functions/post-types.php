<?php
//Creando Post Types
function register_apt_experiencias_post_type() {
	
	register_post_type( 'apt_experiencias', 
		array(
			'labels' => array(
				'menu_name' => 'Experiencias',
				'name' => 'Historias',
				'singular_name' => 'Experiencia',
				'add_new' => 'Nueva historia',
				'add_new_item' => 'Redacta la nueva historia',
				'new_item' => 'Nueva experiencia'
			), 
			'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions'),
			'rewrite' => array( 'slug' => 'experiencias' ),
			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-awards'
		)
	);

	register_post_type( 'apt_enf_rar', 
		array(
			'labels' => array(
				'menu_name' => 'Enfermedades Raras',
				'name' => 'Listado de Patologías',
				'singular_name' => 'Enfermedad Rara',
				'add_new' => 'Nueva Patología',
				'add_new_item' => 'Registrar Patología',
				'new_item' => 'Nueva Patología',
				'edit_item' => 'Editar Patología'
			), 
			'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions'),
			'rewrite' => array( 'slug' => 'enfermedades-raras' ),
			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-sos'
		)
	);

	register_post_type( 'apt_servicios', 
		array(
			'labels' => array(
				'menu_name' => 'Servicios',
				'name' => __('Servicios'),
				'singular_name' => __('Servicio'),
				'add_new' => 'Nuevo servicio',
				'add_new_item' => '',
				'exclude_from_search' => false,
				'new_item' => 'Nueva servicios'
			), 
			'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions'),
			'rewrite' => array( 'slug' => 'servicios' ),
			'public' => true,
			'has_archive' => false,
			'menu_icon' => 'dashicons-editor-expand'
		)
	);

}
add_action('init', 'register_apt_experiencias_post_type');


function alter_apt_enf_rar_query( $query ) {
    if ( $query->is_archive('apt_enf_rar') && $query->is_main_query() ) {
        $query->set( 'orderby', 'post_title' );
        $query->set( 'order', 'ASC' );
        $query->set( 'posts_per_page', 4 );
    }
}
add_action( 'pre_get_posts', 'alter_apt_enf_rar_query' );