<?php

//Colors Picker ACF PRO
function load_javascript_on_admin_edit_post_page() {
	global $parent_file;

  // If we're on the edit post page.
	if (
	  strpos($parent_file, 'post-new.php') !== -1 ||
	  strpos($parent_file, 'edit.php') !== -1 ||
	  strpos($parent_file, 'post.php') !== -1
	) {
		echo "
		  <script>
		  jQuery(document).ready(function(){
			jQuery('.acf-color_picker').iris({
			  palettes: ['#0d9e49', '#77bb1f', '#ff6a00', '#00a0df', '#064DB0', '#8b189b', '#ffffff'],
			  change: function(event, ui){
				jQuery(this).parents('.wp-picker-container').find('wp-picker-holder').find('.iris-palette-container').find('iris-palette').css('background-color', ui.color.toString());
			  }
			});
		  });
		  </script>
		";
	}
}
add_action('in_admin_footer', 'load_javascript_on_admin_edit_post_page');