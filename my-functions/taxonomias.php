<?php

//Creando taxonomias

function apt_servicios_taxonomy() {
	
		$labels_que_er = array( 
				'name' => __('Categorías de Enfermedades Raras'),
				'menu_name' => __('Categorías de ER')
			);
		$que_er = array(
					'labels' => $labels_que_er,
					'public' => true,
					'description' => "Enfermedades Raras",
					'hierarchical' => true,
					'archive' => true,
					'rewrite' => array( 'slug' => 'apt_er')
				);
	register_taxonomy( 'apt_enf_rar_que-son', array('apt_enf_rar'), $que_er);



		$labels_categorias = array( 
				'name' => __('Categorías de Servicios'),
				'menu_name' => __('Categorías')
			);
		$categorias = array(
					'labels' => $labels_categorias,
					'public' => true,
					'hierarchical' => true,
					'archive' => true,
					'rewrite' => array( 'slug' => 'categorias')
				);
	register_taxonomy( 'apt_servicios_categorias', array('apt_servicios'), $categorias);
 
}
add_action( 'init', 'apt_servicios_taxonomy' );

