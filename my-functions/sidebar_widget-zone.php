<?php
//Agregando sidebar/widget-zone
	function asociacion_para_todos_sidebar() {
		register_sidebar(array(
			'name'=> __('Sidebar Principal', 'apt'), 
			'id' => 'sidebar-1',
			'description'   => __( 'Agregar widgets aquí para ser mostrados en la barra lateral de Noticias', 'apt'),
			'before_widget' => '<section id="%1$s" class="widget-container">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>'
		));

		register_sidebar(array(
			'name'=> __('Entradas Populares', 'apt'), 
			'id' => 'sidebar-2',
			'description'   => __( 'Agregar widget de entrada popular', 'apt'),
			'before_widget' => '<section id="%1$s" class="widget-container">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>'
		));

		register_sidebar(array(
			'name'=> __('Últimos Artículos', 'apt'), 
			'id' => 'sidebar-3',
			'description'   => __( 'Agregar widget de ultimos articulos', 'apt'),
			'before_widget' => '<section id="%1$s" class="widget-container">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title experiencias-text">',
			'after_title'   => '</h2>'
		));

	}
	//Agregando widgets al sidebar
	add_action( 'widgets_init', 'asociacion_para_todos_sidebar' );

	//Agregando etiqueta: 'Todas', al Sidebar
	function widget_categories_show_all( $cat_args ) {
  	$cat_args['show_option_all'] = __('Todas', 'apt');
    return $cat_args;
	}

add_filter( 'widget_categories_args', 'widget_categories_show_all', 10, 1 );
