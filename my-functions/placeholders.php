<?php

/**
 * Add placeholder to posts images
 */

function apt_add_image_placeholder($image, $id, $size) {

  if (!$image) {
    $placeholder;

    if ($size === 'thumbnail') {
      $placeholder = array(
        get_template_directory_uri() . '/dist/img/placeholder-500x500.png',
        150,
        150,
        1
      );
    } else {
      $placeholder = array(
        get_template_directory_uri() . '/dist/img/placeholder-837x515.png',
        837,
        515,
        1
      );
    }
    return $placeholder;
  }
  return $image;
}

add_filter( 'wp_get_attachment_image_src', 'apt_add_image_placeholder', 10, 3);
