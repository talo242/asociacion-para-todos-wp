<?php
///// TITULOS 
function apt_title() {

  if ( is_post_type_archive("apt_experiencias") ||
       is_singular('apt_experiencias') ) {

    	return __('Historias que inspiran', 'apt');

  } elseif ( is_post_type_archive('apt_enf_rar') ||
             is_singular( 'apt_enf_rar' ) ) {

  		return __('Enfermedades Raras', 'apt');

  } elseif ( is_page_template( 'donaciones.php' ) ) {

      return __('Donaciones', 'apt');

  } elseif ( is_singular('apt_enf_rar') ) {

    	 return get_the_title();

	} elseif ( is_single() || is_home() ) {

    	 return __('Últimas noticias', 'apt');

	} elseif ( taxonomy_exists( 'apt_servicios_categorias' ) ||
             is_singular( 'apt_servicios' ) ) {

        return __(single_term_title(), 'apt');

  } else {

    	   return get_the_title();

  }
  
}