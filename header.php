<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package asociacion-para-todos
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
	<div class="navbar-animated">
		<div class="navbar-menu">
			<?php the_custom_logo(); ?>
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
				<?php esc_html_e( ' Menu', 'asociacion-para-todos' ); ?>
				<i class="i-menu"></i>
			</button>
		</div>
		<div class="site-navigation">
			<div class="navbar-links">
				<?php wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) ); ?>
			</div>
		</div>
	</div><!-- #site-navigation -->

<div class="all-content">
