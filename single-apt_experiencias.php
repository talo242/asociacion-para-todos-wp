<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package asociacion-para-todos
 */
$featured_img = wp_get_attachment_url( get_post_thumbnail_id(), 'xp-single-header-size' );
get_header(); 
include('sub-header.php');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main single-xp">

			<?php if ($featured_img != null) : ?>
				<div class="single-experiencias-image" style="background-image: url('<?= $featured_img ?>')">
				</div>
			<?php endif ?>

		<div class="single-content">

			<div class="feed single-xp">

				<button class="sidebar-button" aria-controls="widget-area" aria-expanded="false">
					<i class="i-arrow-down"></i>
					<?php esc_html_e( 'Últimos Árticulos', 'widget-area' ); ?>
				</button>

				<div class="sidebar-feed">
					<?php 
						$query = new WP_Query( array( 
							'post_type' => 'apt_experiencias',
							'order' => 'DESC',
							'orderby' => 'date'
							)
						);
					?>

					<div>					
						<ul>
							<?php
							if ( $query->have_posts() ) : ?>
						    <?php while ( $query->have_posts() ) : $query->the_post(); ?>   
						        <li>
						            <a href="<?php asociacion_para_todos_post_link(); ?>">
						            	<?php the_title(); ?>
						            </a>
						        </li>
						    <?php endwhile; ?>
							    <!-- show 404 error here -->
							<?php endif; ?>
						</ul>

					</div>
				</div>

				<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/content-single-apt_experiencias', get_post_type() );
				endwhile; // End of the loop.
				?>

				</div>

				<div class="sidebar-area-right xp-sidebar">
					<?php 
						$query = new WP_Query( array( 
							'post_type' => 'apt_experiencias',
							'order' => 'DESC',
							'orderby' => 'date'
							)
						);
					?>

					<div>
						<h2 class="widget-title experiencias-text">Últimos Artículos</h2>
					
						<ul>
							<?php
							if ( $query->have_posts() ) : ?>
						    <?php while ( $query->have_posts() ) : $query->the_post(); ?>   
						        <li>
						            <a href="<?php asociacion_para_todos_post_link(); ?>">
						            	<?php the_title(); ?>
						            </a>
						        </li>
						    <?php endwhile; ?>
							    <!-- show 404 error here -->
							<?php endif; ?>
						</ul>

					</div>
				</div>

			</div>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php
get_footer();
