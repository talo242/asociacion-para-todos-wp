<?php 
/*
*
*Template Name: Home Page
*
*This Template will display the Home Page.
*
*All in here will show there, be carefuly.
*
*/
get_header();
?>

<div class="home-page">

<?php

	/*
	*Verificando si el Flexible Content 'Secciones', tiene contenido.
	*/
		if ( have_rows('secciones') ) :
			while ( have_rows('secciones') ): the_row();


				/*
				* If the content have a 'Carrousel', it will be displayed.
				*/
					if( get_row_layout() == 'carrousel' ): 
						get_template_part('home-parts/home', 'carrousel' );

				/*
				* If the content have 'Single Blocks', it will be displayed.
				*/
					elseif( get_row_layout() == 'single_block' ):
						get_template_part('home-parts/home', 'single_block' );

				/*
				* If the content have 'Cards', it will be displayed.
				*/
					elseif( get_row_layout() == 'cards' ): 
						get_template_part('home-parts/home', 'cards' );


					endif;

			endwhile;
		endif;

get_footer();
?>
</div>